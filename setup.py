###Use standards for installing the optomechanics folder

from setuptools import setup, find_packages

setup(
    name='optomechanics',
    version='0.1',
    packages=find_packages(),
    author = "ETH Photonics Group",
    author_email = "paseta@ethz.ch",
    description = ("This Python module contains functions and drivers used in and around the particle trapping experiments in the photonics group at ETH."),
)
